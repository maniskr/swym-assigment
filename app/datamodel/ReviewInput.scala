package datamodel

import parser.MetaParser

import scala.concurrent.{ExecutionContext, Future}
import scala.util.matching.Regex

case class ReviewInput(productURL : String) {
  def process()(implicit ec: ExecutionContext) = {
    val productName = getName()
  }

  private def getName() = productURL.split('/')(3).split("-").mkString(" ")
}
