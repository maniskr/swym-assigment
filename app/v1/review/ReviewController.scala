package v1.review

import datamodel.ReviewInput
import javax.inject.Inject
import play.api.Logger
import play.api.data.Form
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

class ReviewController @Inject()(cc: PostControllerComponents)(implicit ec: ExecutionContext)
    extends PostBaseController(cc) {

  private val logger = Logger(getClass)

  private val form: Form[ReviewInput] = {
    import play.api.data.Forms._

    Form(
      mapping(
        "product_url" -> nonEmptyText
      )(ReviewInput.apply)(ReviewInput.unapply)
    )
  }

  def index: Action[AnyContent] = PostAction.async { implicit request =>
    logger.trace("index: ")
   Future(Ok(Json.obj("reviews" -> "No Reviews")))
  }

  def process: Action[AnyContent] = PostAction.async { implicit request =>
    logger.trace("process: ")
    processJsonPost()
  }

  def show(id: String): Action[AnyContent] = PostAction.async { implicit request =>
    logger.trace(s"show: id = $id")
    Future(Ok(Json.obj("reviews" -> "No Reviews")))
  }

  private def processJsonPost[A]()(implicit request: PostRequest[A]): Future[Result] = {
    def failure(badForm: Form[ReviewInput]) = {
      Future.successful(BadRequest(badForm.errorsAsJson))
    }

    def success(reviewInput: ReviewInput) = {
      reviewInput.process()
      Future(Ok(Json.obj("status" -> "success")))
    }

    form.bindFromRequest().fold(failure, success)
  }
}
