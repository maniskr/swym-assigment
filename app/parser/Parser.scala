package parser

import scala.concurrent.ExecutionContext

trait Parser {
  def parse()(implicit ec: ExecutionContext)
}
