package parser

import java.net.URL

import datamodel.MetaData
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element

import scala.concurrent.{ExecutionContext, Future}

case class MetaParser(url : String) {
  def parse()(implicit ec: ExecutionContext): Future[Seq[MetaData]] = {
    //TODO : For production, use Actor model to have lighweight thread to process each request
    Future {
      Jsoup.parse(new URL(url), 10000)
      .getElementsByTag("meta").toArray
        .map {
          data => MetaData(data.asInstanceOf[Element].attr("name"), data.asInstanceOf[Element].attr("content"))
        }
        .toSeq
    }
  }
}
