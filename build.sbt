scalaVersion := "2.12.6"

name := "swym-assigment"

version := "0.1"

libraryDependencies += guice
libraryDependencies += ws
libraryDependencies += "com.netaporter" %% "scala-uri" % "0.4.16"
libraryDependencies += "net.logstash.logback" % "logstash-logback-encoder" % "4.11"
libraryDependencies += "org.mongodb.scala" %% "mongo-scala-driver" % "2.3.0"
libraryDependencies += "net.codingwell" %% "scala-guice" % "4.2.1"
libraryDependencies += "org.jsoup" % "jsoup" % "1.8.3"

enablePlugins(PlayScala)
enablePlugins(DockerPlugin)
